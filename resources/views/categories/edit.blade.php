@extends('index')

@section('content')
    <section class="content d-flex align-items-center forms-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 px-0 m-auto">
                    <div class="card card-primary mb-0">
                        <!--    add new post form   -->
                        <form id="addPostForm" role="form" method="post" action="{{route('categories.update', $category->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group form-floating col-12 px-2">
                                        <label for="title">Category name</label>
                                        <input type="text" id="title" name="name" class="form-control @error('name') is-invalid @enderror" value="{{$category->name}}" placeholder="Enter category name">
                                        @error('title')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" id="submitFormBtn" class="btn btn-outline-blue">@lang('admin.submit')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

