@extends('index')

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6 p-1">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$categoriesCount}}</h3>
                            <p>Categories</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-folder"></i>
                        </div>
                        <a href="{{route('categories.index')}}" class="small-box-footer">@lang('admin.more_info') <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6 p-1">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$projectsCount}}</h3>
                            <p>@lang('admin.new_projects')</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-project-diagram"></i>
                        </div>
                        <a href="{{route('projects.index')}}" class="small-box-footer">@lang('admin.more_info') <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-3 col-6 p-1">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$brandsCount}}</h3>
                            <p>Brands</p>
                        </div>
                        <div class="icon">
                            <i class="fab fa-artstation"></i>
                        </div>
                        <a href="{{route('brands.index')}}" class="small-box-footer">@lang('admin.more_info') <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-3 col-6 p-1">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$totalDownloads}}</h3>
                            <p>Total Downloads</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-download"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <i class="fas fa-arrow-circle-rightw"></i></a>
                    </div>
                </div>

{{--                <div class="col-lg-3 col-6 p-1">--}}
{{--                    <!-- small box -->--}}
{{--                    <div class="small-box bg-info">--}}
{{--                        <div class="inner">--}}
{{--                            <h3>{{$messagesCount}}</h3>--}}
{{--                            <p>Contact Us</p>--}}
{{--                        </div>--}}
{{--                        <div class="icon">--}}
{{--                            <i class="fas fa-address-book"></i>--}}
{{--                        </div>--}}
{{--                        <a href="{{route('admin.contact-us')}}" class="small-box-footer">@lang('admin.more_info') <i class="fas fa-arrow-circle-right"></i></a>--}}
{{--                    </div>--}}
{{--                </div>--}}



            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@endsection

