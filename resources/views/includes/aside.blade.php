<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('dashboard.index')}}" class="brand-link">
        <img src="{{asset('public/template_files/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">@lang('admin.project_name')</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('public/template_files/dist/img/default.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{auth()->user()->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="{{route('categories.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <i class="fa-solid fa-hands-holding-circle"></i>
                        <p>
                            Categories
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('projects.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-project-diagram"></i>
                        <i class="fa-solid fa-hands-holding-circle"></i>
                        <p>
                            @lang('admin.projects')
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('brands.index')}}" class="nav-link">
                        <i class="nav-icon fab fa-artstation"></i>
                        <p>
                            Brands
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.contact-us')}}" class="nav-link">
                        <i class="nav-icon fas fa-address-book"></i>
                        <i class="fa-solid fa-hands-holding-circle"></i>
                        <p>
                            Contact Us
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.logout')}}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            @lang('admin.logout')
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
