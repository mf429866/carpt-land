@extends('index')

@section('content')

    <div class="row" >
        <div class="container bg-light m-2">
            <div class="col-md-12 text-center">
                <a href="{{route('brands.create')}}" class="btn btn-outline-blue"><i class="fas fa-plus ms-1"></i> Add Brand</a>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                @if(count($brands) > 0)
                    <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($brands as $index=>$brand)
                        <tr>
                            <th scope="row">{{$index + 1}}</th>
                            <td>{{$brand->title}}</td>
                            <td>{{$brand->description}}</td>
                            <td><img class="img-thumbnail" width="150px" height="100px;" src="{{$brand->photo}}" alt=""></td>
                            <td>
                                <a href="{{route('brands.edit', $brand->id)}}" class="btn btn-outline-blue"><i class="fas fa-edit ms-1"></i> </a>
                            </td>
                            <td>
                                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#delete_group_{{$brand->id}}"><i class="fas fa-trash  ms-1"></i></button>
                                <!-- delete modal-->
                                <div class="modal fade" id="delete_group_{{$brand->id}}" tabindex="-1" role="dialog" aria-labelledby="delete_shipping" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Delete Brand</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Really need to delete  {{$brand->title}}  !
                                            </div>
                                            <form method="post" action="{{route('brands.destroy',$brand->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-outline-danger">Delete</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>

@endsection
