@extends('index')

@section('content')
    <section class="content d-flex align-items-center forms-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 px-0 m-auto">
                    <div class="card card-primary mb-0">
                        <!--    add new post form   -->
                        <form id="addPostForm" role="form" method="post" action="{{route('brands.update', $brand->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group form-floating col-12 px-2">
                                        <label for="title">Brand Title</label>
                                        <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{$brand->title}}" placeholder="Enter Brand Title">
                                        @error('title')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group form-floating col-12 px-2">
                                        <label for="ticketDesc">Brand Description</label>
                                        <textarea rows="3" id="ticketDesc" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Enter Brand description">{{$brand->description}}</textarea>
                                        @error('description')
                                        <div class="invalid-feedback d-block">{{$message}}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Brand photo</label>
                                        <input type="file" name="photo" value="{{$brand->photo}}" class="form-control-file" id="exampleFormControlFile1">
                                    </div>
                                </div>
                                @error('photo')
                                <div style="color:#dc3545">the photo is required.</div>
                                <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" id="submitFormBtn" class="btn btn-outline-blue">@lang('admin.submit')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

