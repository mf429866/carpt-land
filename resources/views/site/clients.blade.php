<!doctype html>
<!--[if IE 9]> <html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Meta -->
    <title>Our Clients</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="icon" href="{{asset('public/front/images/11.png')}}" />
    <link rel="apple-touch-icon" href="{{asset('public/front/images/apple-touch-icon.png')}}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,700" rel="stylesheet">

    <!-- Custom & Default Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/vendor/nivo-slider/nivo-slider.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/vendor/animate/animate.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/css/not.css')}}">
    <!--    lity    -->
    <script src="{{asset('public/template_files/plugins/lity/lity.js')}}" defer></script>
    <link rel="stylesheet" href="{{asset('public/template_files/plugins/lity/lity.css')}}">
    <!--  noty  -->
    <link rel="stylesheet" href="{{ asset('public/template_files/plugins/noty/noty.css') }}">
    <script src="{{ asset('public/template_files/plugins/noty/noty.min.js') }}"></script>
    <!--===============================================================================================-->
    <link rel="stylesheet" href="{{asset('public/front/css/base.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/css/main.css')}}">
    <script type="text/javascript" src="{{asset('public/front/jquery-3.3.1.min.js')}}"></script>
    <!--Floating WhatsApp css-->
    <link rel="stylesheet" href="{{asset('public/front/floating-wpp.min.css')}}">
    <!--Floating WhatsApp javascript-->
    <script type="text/javascript" src="{{asset('public/front/floating-wpp.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="{{asset('public/front/js/vendor/html5shiv.min.js')}}"></script>
    <script src="{{asset('public/front/js/vendor/respond.min.js')}}"></script>
    <![endif]-->

</head>
<body>

<div id="wrapper">
    <header class="header site-header header-transparent">
        <div class="container">
            @include('includes.message')
            <nav class="navbar navbar-default yamm">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="site-logo" href="{{route('site.index')}}"><img class="img-fluid" width="70" height="60" src="{{asset('public/front/images/11.png')}}" alt="Homepage"></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class=""><a href="{{route('site.index')}}">Home</a></li>
                            <li><a  href="{{route('site.index', "#about")}}"  title="about">About</a></li>
                            <li><a  href="{{route('site.index', "#services")}}" title="about">Why Us</a></li>
                            <li><a  href="{{route('site.index', "#miniServices")}}" title="services">Services</a></li>
                            <li><a  href="{{route('site.index', "#brands")}}" title="brands">Brands</a></li>
                            <li><a class="smoothscroll" href="{{route('clients.index', "#works")}}" title="works">Works</a></li>
                            <li><a class="smoothscroll" href="{{route('clients.index', "#clients")}}" title="clients">Clients</a></li>
                            <li><a href="{{route('clients.index', "#contact")}}" class="smoothscroll" title="contactl">Contact</a></li>
                            <!-- <li><a href="./clients.html#contact" target="_top" title="contact">Contact</a></li> -->

                        </ul>
                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </nav><!-- end nav -->
        </div><!-- end container -->
    </header><!-- end header -->
    <div class="client-page">
        <div class="home-content">

            <div class="row home-content__main">

                <h1 class="h1font">
                    <span style="color:#fff">We Know Flooring Better!</span>
                    <!-- <span style="color:#ED1C24">Breathe</span> -->
                </h1>
                <!-- <div class="home-content__button">
                    <a href="#about" class="smoothscroll btn btn-animatedbg">
                        More About Us
                    </a>
                </div> -->
            </div> <!-- end home-content__main -->

            <!-- <div class="home-content__scroll">
                <a href="#about" class="scroll-link smoothscroll">
                    Scroll
                </a>
            </div> -->

        </div> <!-- end home-content -->
    </div>
    <div class="whats">
        <a title="Talk With Us" href="https://wa.me/+966503606551">
            <img src="{{asset('public/front/images/icons8-whatsapp.gif')}}">
            <!-- <img src="https://img.icons8.com/color/48/000000/whatsapp--v5.png"/> -->
        </a>
    </div>
</div>
<!-- works
================================================== -->
<section id="works" class="s-works target-section">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead">Our Projects</h3>
        </div>
    </div>

    @foreach($categories as $index=>$category)
        @if(count($category->projects) > 0)
        <h3 class="text-center" @if($index == 0) data-aos="fade-up" @endif>{{$category->name}}</h3>
            <div id="{{$index == 0 ? 'slider' : 'slider'.$index}}" class="nivoSlider">
                @foreach($category->projects as $project)
                    <img src="{{$project->photo}}" data-thumb="{{$project->photo}}" title="{{$project->title}}" alt="" />
                @endforeach
            </div>
            @if(! $loop->last)
                <hr id="one">
            @endif
        @endif
    @endforeach

</section> <!-- end s-works -->

<!-- clients
================================================== -->
<section id="clients" class="s-clients target-section">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead" style="color:#fff">Niche Clients</h3>
        </div>
    </div>

    <div class="row clients-list block-1-4 block-tab-1-3 block-mob-1-2" data-aos="fade-up">

        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients2.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients3.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients4.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients5.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients6.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients7.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients8.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients9.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients10.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients11.png')}}" alt="">
            </a>
        </div>
        <div class="col-block item-client">
            <a href="#0">
                <img src="{{asset('public/front/images/clients/clients12.png')}}" alt="">
            </a>
        </div>

    </div> <!-- clients-list -->

</section> <!-- end s-clients -->


<!-- contact
================================================== -->
<section id="contact" class="s-contact target-section">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead">Get In Touch </h3>
            <div class="contact-num">
                <span class="icon-data">+966 50 360 6551<i class="fa fa-phone-square fa-spin" aria-hidden="true"></i></span>
                <span class="icon-data">+966 14 841 3130<i class="fa fa-phone-square fa-spin" aria-hidden="true"></i></span>
            </div>
            <h1 class="display-1">Just contact us now, we are always to ready to help you </h1>
        </div>
    </div>

    <div class="contact1">
        <div class="container-contact1">
            <form class="contact1-form validate-form" action="{{route('contactUs.store')}}" method="POST">
                @csrf
                    <span class="contact1-form-title">
                        Get in touch
                    </span>

                <div class="wrap-input1 validate-input" data-validate="Name is required">
                    <input class="input1" type="text" name="name" value="{{old('name')}}" placeholder="Name">
                    <span class="shadow-input1"></span>
                    @error('name')
                        <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>

                <div class="wrap-input1 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                    <input class="input1" type="text" name="email" value="{{old('email')}}" placeholder="Email">
                    <span class="shadow-input1"></span>
                    @error('email')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>

                <div class="wrap-input1 validate-input" data-validate="Valid phone is required: 01*12345678">
                    <input class="input1" type="text" name="phone" value="{{old('phone')}}" placeholder="Phone number">
                    <span class="shadow-input1"></span>
                    @error('phone')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>

                <div class="wrap-input1 validate-input" data-validate="Subject is required">
                    <input class="input1" type="text" name="subject" value="{{old('subject')}}" placeholder="Subject">
                    <span class="shadow-input1"></span>
                    @error('subject')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>

                <div class="wrap-input1 validate-input" data-validate="Message is required">
                    <textarea class="input1" name="message" placeholder="message">{{ old('message') }}</textarea>
                    <span class="shadow-input1"></span>
                    @error('message')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>

                <div class="container-contact1-form-btn">
                    <button class="contact1-form-btn">
                        <span> Send Email</span>
                    </button>
                </div>
                <div class="contact-icons text-center">
                        <span class="icons-info">
                            <a target="_blank" href="https://www.facebook.com/CARPETLANDKSA/?ti=as" ><i class="fab fa-facebook fa-lg"></i></a>
                            <a target="_blank" href="https://www.linkedin.com/company/carpetlandksa"><i class="fab fa-linkedin"></i></a>
                            <a target="_blank" href="https://wa.me/+966503606551"><i class="fab fa-whatsapp"></i></a>
                            <!-- <a href=""><i class="fab fa-instagram fa-lg"></i></a> -->
                        </span>
                </div>
            </form>
            <div class="col-full download" style="text-align: center; margin-top: 25px;">
                <a href="{{route('site.index')}}"><i class="fa fa-home"></i> Back To Home</a>
            </div>
        </div>
    </div>

</section> <!-- end s-contact -->


<footer>
    <div class="row">
        <div class="col-full cl-copyright">
					<span>
						Copyright &copy;
						<script>document.write(new Date().getFullYear());</script> All rights reserved
						 by : &nbsp;<a
                            href="#" target="_blank"> CARPET LAND</a>
					</span>

        </div>
    </div>

    <div class="cl-go-top">
        <a title="Back to Top" href="#top" onclick="topFunction()" id="myBtn"></a>
    </div>
</footer>
</div><!-- end wrapper -->

<!-- jQuery Files -->
<script src="{{asset('public/front/js/jquery.min.js')}}"></script>
<script src="{{asset('public/front/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/front/js/parallax.js')}}"></script>
<script src="{{asset('public/front/js/animate.js')}}"></script>
<script src="{{asset('public/front/js/custom.js')}}"></script>
<script src="{{asset('public/front/js/main.js')}}"></script>
<script src="{{asset('public/front/js/top.js')}}"></script>
<script src="{{asset('public/front/js/service.js')}}"></script>
<script src="{{asset('public/front/vendor/nivo-slider/jquery.nivo.slider.js')}}"></script>
</body>
</html>
