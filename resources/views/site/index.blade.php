<!DOCTYPE html>
<html class="no-js" lang="en">

<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Carpet Land</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="{{asset('public/front/images/11.png')}}">


    <!-- impo codes ad by MohaRamaX 2019 -->

    <!-- [ Social Media Meta Tag ] -->
    <link href='https://plus.google.com/xxxxx/posts' rel='publisher' />
    <link href='https://plus.google.com/xxxxx/about' rel='author' />
    <link href='https://plus.google.com/xxxxx' rel='me' />
    <meta content='#' name='google-site-verification' />
    <meta content='#' name='msvalidate.01' />
    <meta content='#' name='p:domain_verify' />
    <meta content='#' name='yandex-verification' />
    <meta content='#' name='alexaVerifyID' />
    <meta content='EGYPT' name='geo.placename' />
    <meta content='#' name='Author' />
    <meta content='general' name='rating' />
    <meta content='ar-EG' name='geo.country' />
    <meta content='https://www.facebook.com/#' property='article:author' />
    <meta content='https://www.facebook.com/#' property='article:publisher' />
    <meta content='#' property='fb:app_id' />
    <meta content='#' property='fb:admins' />
    <meta content='ar_EG' property='og:locale' />
    <meta content='ar_EG' property='og:locale:alternate' />
    <meta content='en_US' property='og:locale:alternate' />
    <meta content='summary' name='twitter:card' />
    <meta expr:content='data:blog.pageTitle' name='twitter:title' />
    <meta content='@CarpetLand' name='twitter:site' />
    <meta content='@Moha###' name='twitter:creator' />
    <meta content='#ed1c24' name='msapplication-navbutton-color' />
    <meta content='#ed1c24' name='apple-mobile-web-app-status-bar-style' />
    <meta content='#ed1c24' name='theme-color' />
    <!-- [ Social Media Meta Tag END ] -->

    <!-- [ Meta Tag SEO ] -->
    <link href='//1.bp.blogspot.com' rel='dns-prefetch' />
    <link href='//28.2bp.blogspot.com' rel='dns-prefetch' />
    <link href='//3.bp.blogspot.com' rel='dns-prefetch' />
    <link href='//4.bp.blogspot.com' rel='dns-prefetch' />
    <link href='//2.bp.blogspot.com' rel='dns-prefetch' />
    <link href='//www.blogger.com' rel='dns-prefetch' />
    <link href='//maxcdn.bootstrapcdn.com' rel='dns-prefetch' />
    <link href='//fonts.googleapis.com' rel='dns-prefetch' />
    <link href='//use.fontawesome.com' rel='dns-prefetch' />
    <link href='//ajax.googleapis.com' rel='dns-prefetch' />
    <link href='//resources.blogblog.com' rel='dns-prefetch' />
    <link href='//www.facebook.com' rel='dns-prefetch' />
    <link href='//plus.google.com' rel='dns-prefetch' />
    <link href='//twitter.com' rel='dns-prefetch' />
    <link href='//www.youtube.com' rel='dns-prefetch' />
    <link href='//feedburner.google.com' rel='dns-prefetch' />
    <link href='//www.pinterest.com' rel='dns-prefetch' />
    <link href='//www.linkedin.com' rel='dns-prefetch' />
    <link href='//feeds.feedburner.com' rel='dns-prefetch' />
    <link href='//github.com' rel='dns-prefetch' />
    <link href='//player.vimeo.com' rel='dns-prefetch' />
    <link href='//www.dailymotion.com' rel='dns-prefetch' />
    <link href='//platform.twitter.com' rel='dns-prefetch' />
    <link href='//apis.google.com' rel='dns-prefetch' />
    <link href='//connect.facebook.net' rel='dns-prefetch' />
    <link href='//cdnjs.cloudflare.com' rel='dns-prefetch' />
    <link href='//www.google-analytics.com' rel='dns-prefetch' />
    <link href='//pagead2.googlesyndication.com' rel='dns-prefetch' />
    <link href='//googleads.g.doubleclick.net' rel='dns-prefetch' />
    <link href='//www.gstatic.com' rel='preconnect' />
    <link href='//www.googletagservices.com' rel='dns-prefetch' />
    <link href='//static.xx.fbcdn.net' rel='dns-prefetch' />
    <link href='//tpc.googlesyndication.com' rel='dns-prefetch' />
    <link href='//syndication.twitter.com' rel='dns-prefetch' />


    <b:if cond='data:blog.url == data:blog.homepageUrl'>
        <meta content='######' name='keywords' />
    </b:if>

    <b:if cond='data:blog.metaDescription'>
        <meta expr:content='data:blog.metaDescription' property='og:description' />
        <b:else />
        <meta content='#####' property='og:description' />
    </b:if>


    <!--Browser Caching-->
    <include expiration='7d' path='*.css' />
    <include expiration='7d' path='*.js' />
    <include expiration='3d' path='*.gif' />
    <include expiration='3d' path='*.jpeg' />
    <include expiration='3d' path='*.jpg' />
    <include expiration='3d' path='*.png' />
    <meta content='tue, 02 jun 2022 00:00:00 GMT' http-equiv='expires' />



    <!-- End Of Codes -->


    <!-- CSS
    ================================================== -->
    <!--===============================================================================================-->
    <!--<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">-->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Dosis:600&display=swap" rel="stylesheet">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/vendor/animate/animate.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/css/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/vendor/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/vendor/nivo-slider/nivo-slider.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/css/not.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="{{asset('public/front/css/base.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/css/vendor.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/css/main.css')}}">
    <!--JQuery-->
    <script type="text/javascript" src="{{asset('public/front/jquery-3.3.1.min.js')}}"></script>
    <!--Floating WhatsApp css-->
    <link rel="stylesheet" href="{{asset('public/front/floating-wpp.min.css')}}">
    <!--Floating WhatsApp javascript-->
    <script type="text/javascript" src="{{asset('public/front/floating-wpp.js')}}"></script>



    <!-- script
    ================================================== -->
    <script src="{{asset('public/front/js/modernizr.js')}}"></script>
    <script src="{{asset('public/front/js/pace.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{asset('public/front/js/service.js')}}"></script>
    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{asset('public/front/images/11.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('public/front/images/11.png')}}" type="image/x-icon">
    <!-- <style>
        @font-face {
            font-family: BuanaRegular;
            src: url(font/BuanaRegular.ttf);
        }

        @font-face {
            font-family: ict;
            src: url(font/ITCAvantGardeStd-XLt.otf);
        }
    </style> -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!-- <link href="https://fonts.googleapis.com/css2?family=Cairo&display=swap" rel="stylesheet"> -->

</head>

<body id="top">

<!-- preloader
================================================== -->
<div id="preloader">
    <div id="loader" class="dots-jump">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>


<!-- header
================================================== -->
<header class="s-header">

    <div class="row">
        <div class="header-logo">
            <a class="site-logo" href="{{route('site.index')}}"><img class="img-fluid" width="80" height="70" src="{{asset('public/front/images/11.png')}}" alt="Homepage"></a>
        </div>
        <nav class="header-nav-wrap">
            <ul class="header-nav">
                <li class="current"><a class="smoothscroll" href="{{route('site.index', "#home")}}" title="home">Home</a></li>
                <li><a class="smoothscroll" href="{{route('site.index', "#about")}}"  title="about">About</a></li>
                <li><a class="smoothscroll" href="{{route('site.index', '#services')}}" title="about">Why Us</a></li>
                <li><a class="smoothscroll" href="{{route('site.index', '#miniServices')}}" title="services">Services</a></li>
                <li><a class="smoothscroll" href="{{route('site.index', '#brands')}}" title="brands">Brands</a></li>
                <li><a href="{{route('clients.index', '#clients')}}" target="_top" title="client">Clients</a></li>
                <li><a  href="{{route('clients.index', '#works')}}" target="_top" title="works">Works</a></li>
                <li><a href="{{route('clients.index', '#contact')}}" target="_top" title="contact">Contact</a></li>
            </ul>
        </nav> <!-- end header-nav-wrap -->
        <a class="header-menu-toggle" href="#0">
            <span class="header-menu-icon"></span>
        </a>
    </div> <!-- end row -->
</header> <!-- end s-header -->
<!-- <li><a href="./clients.html#clients" target="_top" title="client">Clients</a></li>
<li><a  href="./clients.html#works" target="_top" title="works">Works</a></li>
<li><a href="./clients.html#contact" target="_top" title="contact">Contact</a></li> -->
<!-- home
================================================== -->
<section id="home" class="s-home page-hero target-section m-auto" data-parallax="scroll" data-image-src="{{asset('public/front/images/hom.jpg')}}"
         data-natural-width=3000 data-natural-height=2000 data-position-x="center" data-position-y="center">

    <!-- <div class="shadow-overlay"></div> -->

    <div class="home-content">

        <div class="row home-content__main">

            <h1 class="h1font">
                <span style="color:#fff">We Know Flooring Better!</span>
                <!-- <span style="color:#ED1C24">Breathe</span> -->
            </h1>
            <!-- <div class="home-content__button">
                <a href="#about" class="smoothscroll btn btn-animatedbg">
                    More About Us
                </a>
            </div> -->
        </div> <!-- end home-content__main -->

        <!-- <div class="home-content__scroll">
            <a href="#about" class="scroll-link smoothscroll">
                Scroll
            </a>
        </div> -->

    </div> <!-- end home-content -->

    <ul class="home-social">
        <li>
            <a target="_blank" href="https://www.facebook.com/CARPETLANDKSA/?ti=as"><i class="fab fa-facebook-f"
                                          aria-hidden="true"></i><span>Facebook</span></a>
        </li>
        <!-- <li>
            <a target="_blank" href=""><i class="fab fa-twitter"
                    aria-hidden="true"></i><span>Twiiter</span></a>
        </li> -->
        <!-- <li>
            <a target="_blank" href=""><i class="fab fa-instagram"
                    aria-hidden="true"></i><span>Instagram</span></a>
        </li> -->
        <li>
            <a target="_blank" href="https://www.linkedin.com/company/carpetlandksa"><i class="fab fa-linkedin"
                                          aria-hidden="true"></i><span>LinkedIn</span></a>
        </li>
        <li>
            <a target="_blank" href="https://wa.me/+966503606551">
                <i class="fab fa-whatsapp" aria-hidden="true"></i><span>WhatsApp</span></a>
        </li>
        <!-- <li>
            <a target="_blank" href="https://www.youtube.com/channel/UCGmmPy_ax2VX7i9CZZ2nI3A?view_as=subscriber"><i
                    class="fab fa-youtube" aria-hidden="true"></i><span>Youtube</span></a>
        </li> -->
    </ul> <!-- end home-social -->

</section> <!-- end s-home -->


<!-- about
================================================== -->
<section id="about" class="s-about target-section">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3  class="subhead">About Us</h3>
            <h1 class="display-1">
                Carpet land, a Creative weaving carpet-Flooring provider </h1>
            <p class="lead">
                We pride ourselves on taking the time to guide our customers through the entire process, helping
                them make the very best decisions possible on new floor coverings, blinds, shutters and awnings. We
                do it everyday, and it’s why our customer satisfaction is so high. </p>
            <p class="lead">
                From initial design ideas right through to installation, we’ll show you how to find the perfect
                solution that suits your style, and budget. </p>

            <!-- <div class="col-full" style="text-align: center">
                <a href="clients.html" class="butonn">Let See What We Can Do For You</a>
            </div> -->
        </div>
    </div>

</section> <!-- end s-about -->


<!-- Why Us
================================================== -->
<section id="services" class="s-services target-section">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead" style="color: red;">Why Us</h3>
            <p class="lead lead-light">
                Carpet Land has a creative design team to analyze your needs into a piece of art.
                We provide your organization with the international brands materials in a very cost-effective and a
                time effective manner.
                We have the widest range, including carpets of all styles, types and colours, ceramic tiles,
                engineered timber and laminate flooring.
            </p>
            <div class="col-full download" style="text-align: center">
                <a id="pdfFile"  href="{{asset('public/front/images/carpet Land Profile Small (2).pdf')}}" download="carpet Land Profile">Download Our Company Profile</a>
            </div>
        </div>
    </div>

    <div class="policity">
        <div class="vision wow flipInX">
            <h1>Our Vision</h1>
            <p>Our vision is a global concept of hospitalization.</p>
        </div>

        <div class="mission">
            <h1>Our Mission</h1>
            <p>To provide large organizations in Saudi Arabia with international carpet brands materials in a
                cost-effective.</p>
        </div>

        <div class="values">
            <h1>Our Values</h1>
            <p>We believe in creativity and uniqueness
                We believe in client satisfaction
                we have a well listener team to your needs.</p>
        </div>

        <div class="philo">
            <h1>Our Philosophy</h1>
            <p>inspire customers, interior designers
                (Discover the fascination Get Designing).
            </p>
        </div>
    </div>
</section> <!-- end s-services -->

<section id="miniServices" class="target-section">
    <div class="main-content row section-header" data-aos="fade-up">
        <div class="row section-header" data-aos="fade-up">
            <div class="col-full">
                <h3 class="subhead">Services</h3>
            </div>
        </div>
        <!-- <div class="services-content">
            <div>
                <h3>Design <br> Consultation</h3>
                <p>Just contact us, and our team will offer you the best design consultation service at no cost.</p>
            </div>
            <div>
                <h3> CAD <br> Planning </h3>
                <p>We work on high profile projects. Our CAD planners work craftily with the sales and design team to ensure you the professional delivery service on time.</p>
            </div>
            <div>
                <h3>Carpet-Flooring Installation</h3>
                <p>Our Professional Carpet Installation service makes huge differences in the look and feel of your new carpet.
                No matter the carpeting service you need, our team is always ready to help you.
                    We guarantee a stress free installation process for you. Our installation team receives updated international training courses in many countries every year. Carpet Land is licensed to conduct installation services. </p>
            </div>
        </div> -->
        <div class="row about-process block-1-2 block-tab-full">

            <ul class="row icons wow flash">
                <li class="branding wow shake">
                    <img src="{{asset('public/front/images/icons/consulting.png')}}"/>
                </li>

                <li class="digital wow shake">
                    <img src="{{asset('public/front/images/icons/CAD Planning.svg')}}"/>
                </li>

                <li class="design wow shake">
                    <img src="{{asset('public/front/images/icons/150-180w.png')}}"/>
                </li>

            </ul>

            <div class="row panel col-full" id="br-panel">
                <div class="item-process">
                    <div class="item-process__header">
                        <h3 class="brand">Design Consultation</h3>
                    </div>
                    <p style="margin-top: 47px;">Just contact us, and our team will offer you the best design consultation service at no cost.</p>
                </div>
            </div>

            <div class="row panel" id="dg-panel">
                <div class="item-process" >
                    <div class="item-process__header">
                        <h3 class="digi">CAD Planning</h3>
                    </div>
                    <p style="margin-top: 47px;">We work on high profile projects. Our CAD planners work craftily with the sales and design team to ensure you the professional delivery service on time.</p>
                    </p>
                </div>
            </div>

            <div class="row panel" id="wb-panel">
                <div class="item-process">
                    <div class="item-process__header">
                        <h3 class="wbdesign">Carpet-Flooring Installation</h3>
                    </div>
                    <p style="margin-top: 65px;">Our Professional Carpet Installation service makes huge differences in the look and feel of your new carpet.
                        No matter the carpeting service you need, our team is always ready to help you.
                        We guarantee a stress free installation process for you. Our installation team receives updated international training courses in many countries every year. Carpet Land is licensed to conduct installation services. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Brands
================================================== -->
<section id="brands" class="s-works target-section">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead">Brands</h3>
        </div>
    </div>

    <div class="portfolio block-1-4 block-m-1-3 block-tab-1-2 collapse">

        @foreach($brands as $brand)
            <div class="col-block item-folio" data-aos="fade-up">

            <div class="item-folio__thumb">
                <a href="{{$brand->photo}}" class="thumb-link" title="Brinton" data-size="1050x700">
                    <img src="{{$brand->photo}}" srcset="{{$brand->photo}}" alt="">
                </a>
            </div>

            <div class="item-folio__text">
                <h3 class="item-folio__title">
                    {{$brand->title}}
                </h3>
                <p class="item-folio__cat">{{ $brand->description }}</p>
            </div>

            <div class="item-folio__caption">
                <h3>BRINTONS SERVICES ـــــــــــــــــــــ </h3>
                <div class="brinton-services">
                    <span>Designs</span>
                    <span>sampling</span>
                    <span>Project Management</span>
                </div>
            </div>

        </div> <!-- end item-folio -->
        @endforeach

    </div> <!-- end portfolio -->
    <div class="col-full download" style="text-align: center; margin-top: 25px;">
        <a href="{{route('clients.index')}}"> Discover Our Site</a>
    </div>

</section>

<!-- </section> end s-stats -->
<div class="whats">
    <a title="Talk With Us" href="https://wa.me/+966503606551">
        <img src="{{asset('public/front/images/icons8-whatsapp.gif')}}">
        <!-- <img src="https://img.icons8.com/color/48/000000/whatsapp--v5.png"/> -->
    </a>
</div>

<!-- footer
================================================== -->
<footer>
    <div class="row">
        <div class="col-full cl-copyright">
                <span>
                    Copyright &copy;
                    <script>document.write(new Date().getFullYear());</script> All rights reserved
                      by : &nbsp;<a
                        href="#" target="_blank"> CARPET LAND</a>
                </span>

        </div>
    </div>

    <div class="cl-go-top">
        <a class="smoothscroll" title="Back to Top" href="#top" onclick="topFunction()" id="myBtn"></a>
    </div>
</footer>


<!-- photoswipe background
================================================== -->
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">

    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">

        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div><button class="pswp__button pswp__button--close"
                                                         title="Close (Esc)"></button> <button class="pswp__button pswp__button--share"
                                                                                               title="Share"></button> <button class="pswp__button pswp__button--fs"
                                                                                                                               title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom"
                                                                                                                                                                           title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>

    </div>

</div><!-- end photoSwipe background -->


<!-- Java Script
================================================== -->
<script src="{{asset('public/front/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('public/front/js/plugins.js')}}"></script>
<script src="{{asset('public/front/js/main.js')}}"></script>
<script src="{{asset('public/front/js/top.js')}}"></script>
<script src="{{asset('public/front/js/conta.js')}}"></script>
<script src="{{asset('public/front/js/tilt.jquery.min.js')}}"></script>
<script src="{{asset('public/front/vendor/nivo-slider/jquery.nivo.slider.js')}}"></script>
<script>

    $('#pdfFile').on('click', e => {

        $.ajax({
            url: "{{route('increase-total-downloads')}}",
            type: "GET",
            data: {},
            success: function(data) {}
        });
    });
</script>
</body>

</html>
