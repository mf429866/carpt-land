@extends('index')

@section('content')

    <div class="row">
{{--        <div class="container bg-light m-2">--}}
{{--            <div class="col-md-12 text-center">--}}
{{--                <h3> Contact Us Messages</h3>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="col-12">
            <div class="card">
                @if(count($messages) > 0)
                    <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Subject</th>
                        <th scope="col">Message</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $index=>$message)
                        <tr>
                            <th scope="row">{{$index + 1}}</th>
                            <th scope="row">{{$message->name}}</th>
                            <th scope="row">{{$message->email}}</th>
                            <th scope="row">{{$message->phone}}</th>
                            <th scope="row">{{$message->subject}}</th>
                            <th scope="row">{{$message->message}}</th>
                            <td>
                                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#delete_group_{{$message->id}}"><i class="fas fa-trash  ms-1"></i></button>
                                <!-- delete modal-->
                                <div class="modal fade" id="delete_group_{{$message->id}}" tabindex="-1" role="dialog" aria-labelledby="delete_shipping" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Delete Message</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Really need to delete this message !
                                            </div>
                                            <form method="post" action="{{route('admin.contact-us.destroy',$message->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-outline-danger">Delete</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                    <div class="container bg-light">
                        <div class="col-md-12 text-center">
                            <h3> No Message to Show</h3>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
