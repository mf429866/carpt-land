<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([
    'prefix' => 'admin',
], function () {
    // get login
    Route::get('/login',[App\Http\Controllers\Admin\LoginController::class, 'login'])->name('login');
    // post login
    Route::post('/login',[App\Http\Controllers\Admin\LoginController::class, 'doLogin'])->name('admin.login');

    Route::group([
        'middleware' => 'auth'
    ], function () {
        // dashboard
        Route::get('/',[App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard.index');
        // logout
        Route::any('/logout',[App\Http\Controllers\Admin\LoginController::class, 'logout'])->name('admin.logout');
        // projects
        Route::resource('/projects',App\Http\Controllers\Admin\ProjectsController::class);
        // brands
        Route::resource('/brands',App\Http\Controllers\Admin\BrandsController::class);
        // categories
        Route::resource('/categories',App\Http\Controllers\Admin\CategoriesController::class);
        // contact us
        Route::get('/contact-us',[App\Http\Controllers\Admin\ContactUsController::class, 'index'])->name('admin.contact-us');
        // delete contact-us messages
        Route::delete('/contact-us/{id}',[App\Http\Controllers\Admin\ContactUsController::class, 'destroy'])->name('admin.contact-us.destroy');
    });
});

//Route::get('/',[App\Http\Controllers\Site\FileController::class, 'index'])->name('site.index');

//Route::get('/',[App\Http\Controllers\Site\ContactUsController::class, 'index'])->name('site.index');

Route::get('/',[App\Http\Controllers\Site\SiteController::class, 'index'])->name('site.index');

Route::get('/clients',[App\Http\Controllers\Site\SiteController::class, 'clients'])->name('clients.index');

Route::post('/clients',[App\Http\Controllers\Site\ContactUsController::class, 'store'])->name('contactUs.store');

Route::get('/increase-total-downloads',[App\Http\Controllers\Site\SettingsController::class, 'increaseTotalDownload'])->name('increase-total-downloads');
