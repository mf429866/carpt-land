<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Contracts\View\View;

class SettingsController extends Controller
{

    public function increaseTotalDownload()
    {
        $setting = Setting::firstOrFail();
        ++ $setting->totalDownloads;
        $setting->save();
        return "success";
    }
}
