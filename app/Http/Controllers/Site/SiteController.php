<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Contracts\View\View;

class SiteController extends Controller
{
    /**
     * open login page.
     *
     * @return View
     */
    public function index(): View
    {
        $brands = Brand::all();
        return view('site.index', compact('brands'));
    }

    /**
     * open login page.
     *
     * @return View
     */
    public function clients(): View
    {
        $categories = Category::all();
        return view('site.clients', compact('categories'));
    }
}
