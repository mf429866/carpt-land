<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;

class FileController extends Controller
{
    /**
     * open login page.
     *
     * @return View
     */
    public function index(): View
    {
        return view('site.index');
    }
}
