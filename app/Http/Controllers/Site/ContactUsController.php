<?php

namespace App\Http\Controllers\Site;

use App\Models\ContactUs;
use Illuminate\Contracts\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\ContactUsRequest;

class ContactUsController extends Controller
{
    /**
     * open login page.
     *
     * @return View
     */
    public function index(): View
    {
        return view('site.index');
    }

    /**
     * add contact us message
     *
     * @param ContactUsRequest $request
     * @return RedirectResponse
     */
    public function store(ContactUsRequest $request)
    {
        ContactUs::create($request->validated());
        return redirect()->route('clients.index')->with('success', 'your messages added successfully');
    }
}
