<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContactUs;
use Illuminate\Contracts\View\View;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    /**
     * open login page.
     *
     * @return View
     */
    public function index(): View
    {
        $messages = ContactUs::all();
        return view('contactUs.index', compact('messages'));
    }

    public function destroy($id)
    {
        ContactUs::findOrFail($id)->delete();
        return back()->with('success', 'Message has been deleted successfully');
    }
}
