<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BrandsRequest;
use App\Http\Requests\ProjectsRequest;
use App\Http\Requests\UpdateBrandsRequest;
use App\Http\Requests\UpdateProjectsRequest;
use App\Models\Brand;
use App\Models\Project;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $brands = Brand::all();
        return view('brands.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandsRequest $request
     * @return RedirectResponse
     */
    public function store(BrandsRequest $request)
    {
        $data = $request->validated();
        unset($data['photo']);
        if (request('photo')) {
            $this->storePhoto(request('photo'), 'brands');
            $data['photo'] = request('photo')->hashName();
        }
        Brand::create($data);
        return redirect()->route('brands.index')->with('success', 'Brand add successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Brand $brand
     * @return Factory|View
     */
    public function edit(Brand $brand): View
    {
        return view('brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBrandsRequest $request
     * @param Brand $brand
     * @return RedirectResponse
     */
    public function update(UpdateBrandsRequest $request, Brand $brand)
    {
        $data = $request->validated();
        unset($data['photo']);
        if (request('photo')) {
            $this->updatePhoto(request('photo'), $brand->photo,  'brands');
            $data['photo'] = request('photo')->hashName();
        }
        $brand->update($data);
        return redirect()->route('brands.index')->with('success', 'Brand updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $brand
     * @return RedirectResponse
     */
    public function destroy(Brand $brand)
    {
        $this->deletePhoto($brand->photo, 'brands');
        $brand->delete();
        return redirect()->route('brands.index')->with('success', 'Brand deleted successfully');
    }
}
