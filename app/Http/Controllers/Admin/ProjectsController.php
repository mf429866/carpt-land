<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectsRequest;
use App\Http\Requests\UpdateProjectsRequest;
use App\Models\Category;
use App\Models\Project;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $projects = Project::all();
        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $categories = Category::all();
        return view('projects.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectsRequest $request
     * @return RedirectResponse
     */
    public function store(ProjectsRequest $request)
    {
        $data = $request->validated();
        unset($data['photo']);
        if (request('photo')) {
            $this->storePhoto(request('photo'), 'projects');
            $data['photo'] = request('photo')->hashName();
        }
        Project::create($data);
        return redirect()->route('projects.index')->with('success', 'project add successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return Factory|View
     */
    public function edit(Project $project): View
    {
        $categories = Category::all();
        return view('projects.edit', compact('project', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProjectsRequest $request
     * @param Project $project
     * @return RedirectResponse
     */
    public function update(UpdateProjectsRequest $request, Project $project)
    {
        $data = $request->validated();
        unset($data['photo']);
        if (request('photo')) {
            $this->updatePhoto(request('photo'), $project->photo,  'projects');
            $data['photo'] = request('photo')->hashName();
        }
        $project->update($data);
        return redirect()->route('projects.index')->with('success', 'project updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Project $project
     * @return RedirectResponse
     */
    public function destroy(Project $project)
    {
        $this->deletePhoto($project->photo, 'projects');
        $project->delete();
        return redirect()->route('projects.index')->with('success', 'project deleted successfully');
    }
}
