<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class LoginController extends Controller
{
    /**
     * open login page.
     *
     * @return View
     */
    public function login(): View
    {
        return view('login.index');
    }

    /**
     * admin login
     *
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function doLogin(LoginRequest $request): RedirectResponse
    {

        if (auth()->guard('web')->attempt($request->validated())) {
            return redirect()->route('dashboard.index')->with('success', 'تم تسجيل الدخول بنجاح');
        }

        return redirect()->route('admin.login')->with('error', 'تاكد من صحة البيانات');
    }

    /**
     * admin logout
     *
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        auth('web')->logout();

        return redirect()->route('admin.login')->with('success', 'تم تسجيل الخروج بنجاح');
    }
}
