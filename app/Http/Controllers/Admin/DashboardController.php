<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\ContactUs;
use App\Models\Project;
use App\Models\Setting;
use Illuminate\Contracts\View\View;

class DashboardController extends Controller
{
    /**
     * open login page.
     *
     * @return View
     */
    public function index(): View
    {
        $projectsCount = Project::count();
        $categoriesCount = Category::count();
        $brandsCount = Brand::count();
        $messagesCount = ContactUs::count();
        $totalDownloads = Setting::firstOrFail()->totalDownloads;
        return view('dashboard.index', compact('projectsCount', 'messagesCount', 'categoriesCount', 'brandsCount', 'totalDownloads'));
    }
}
